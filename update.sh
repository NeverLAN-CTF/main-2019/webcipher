#!/bin/bash
UPDATEEVERY=30
WORDSFILE="/var/www/html/wordsUsed.txt"
CIPHERFILE="/var/www/html/cipher.txt"

echo "loading words"
LINES=`cat $WORDSFILE`

ceaser () {
    local OPTIND
    local encrypt n=0
    while getopts :edn: option; do
        case $option in
            e) encrypt=true ;;
            d) encrypt=false ;;
            n) n=$OPTARG ;;
            :) echo "error: missing argument for -$OPTARG" >&2
               return 1 ;;
            ?) echo "error: unknown option -$OPTARG" >&2
               return 1 ;;
        esac
    done
    shift $((OPTIND-1))
    if [[ -z $encrypt ]]; then
        echo "error: specify one of -e or -d" >&2
        return 1
    fi
 
    local upper=ABCDEFGHIJKLMNOPQRSTUVWXYZ
    local lower=abcdefghijklmnopqrstuvwxyz
    if $encrypt; then
        tr "$upper$lower" "${upper:n}${upper:0:n}${lower:n}${lower:0:n}" <<< "$1"
    else
        tr "${upper:n}${upper:0:n}${lower:n}${lower:0:n}" "$upper$lower" <<< "$1"
    fi
}

#echo "building array"
for line in $LINES;do
	WORDS[$X]="$line";
	((X=X+=1))
done
#echo "done with words"
 
while true; do
	NUM=$(( ( RANDOM % 25 )  + 1 ))
	SECNUM=$(( ( RANDOM % ${#WORDS[@]}) ))
	IN=${WORDS[$SECNUM]}
	OUTPUT=$(ceaser -e -n $NUM $IN)' '$IN
	echo $OUTPUT > ${CIPHERFILE}
	#echo "going to sleep"
	sleep $UPDATEEVERY
done




