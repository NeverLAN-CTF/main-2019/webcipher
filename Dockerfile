FROM neverlanctf/base:4th-year

MAINTAINER Zane Durkin <zane@neverlanctf.org>

# import web files
COPY web/ /var/www/html
COPY update.sh /var/www/update.sh
COPY init.sh /var/www/init.sh

CMD ["sh", "/var/www/init.sh"]
#FROM php:5.6-apache
#
#    MAINTAINER Zane Durkin <zane@zemptech.com>
#
#	RUN apt-get update --fix-missing
#	RUN docker-php-ext-install mysqli
#	RUN a2enmod rewrite
#	
#    COPY web/ /var/www/html/
#	COPY update.sh /var/www/update.sh
#	COPY init.sh /var/www/init.sh
#	
#	CMD ["sh", "/var/www/init.sh"]
