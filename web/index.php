<?php
$key="flag{H3llo_c4es3r}";
$bruteCount = 10;
session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>anti-human cipher</title>
	</head>
	<body>
		<?php
			$file = file_get_contents('cipher.txt');
			$cipher = explode(' ', $file);
			if(array_key_exists('deciphered', $_GET) && $_GET['deciphered']!=""){
				
				if($_SESSION['count'] > $bruteCount){ // keep track of brute forcing
					?>
						<h1>Please find the anwser before submitting</h1>
						<p>reload to continue</p>
						</body>
						</html>
					<?php
					$_SESSION['count'] = 0;
					die();
				}else{
					$_SESSION['count'] += 1;
				}
			
				if(trim($cipher[1]) == trim($_GET['deciphered'])){
					?>
						<div id="key">
							<h1>You got it!<h1>
							<p><?php echo($key);?></p>
						</div>
						</body>
						</html>
					<?php
					exit();// kill the page so the form isn't shown again
				}// else, form is shown
			}// else, form is shown
		?>
		<div id="cipher_box">
			<h1 id="cipher"><?php echo($cipher[0]);?></h1>
		</div>
		<div id="form_box">
			<form id="form" method="GET" action="?">
				<input type="text" name="deciphered" id="deciphered"/>
				<input type="submit"/>
			</form>
		</div>
	</body>
</html>
