# WebCipher

#### Description
To verify that only computers can access the website, you must reverse the caeser cipher
There are a list of possible words that the cipher may be [here](web/wordsUsed.txt) 

#### Flag
flag{H3llo_c43s3r}

#### Hints
The answer changes to fast to be done by human hand. Try making a script to do it for you
